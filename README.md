# SonarQube

## Inleiding

SonarQube (voorheen Sonar) is een opensource platform om code te kunnen inspecteren. Dit kan zowel handmatig als volledig automatisch in een Continuous Integration Environment. Het is vooral bedoeld om ontwikkelaars goede kwaliteit code te laten opleveren, maar kan uiteraard ook door (functionele/technische) testers worden gebruikt. Met behulp van  SonarQube kan er snel 'onder de motorkap' gekeken worden om bijvoorbeeld complexe stukken code te identificeren die extra testgevallen/aandacht nodig hebben, of wellicht in aanmerking dienen te komen om te refactoren.

### Vele bekende programmeertalen worden ondersteund, zoals:

* C#
* Go
* Java
* JavaScript
* Python
* TypeScript

Volledige overzicht van ondersteunde talen: https://www.sonarqube.org/features/multi-languages/

### Enkele voorbeelden van rapportages die kunnen worden gegenereerd zijn:

* bugs
* code duplicaties
* afwijking op coderings standaarden
* code coverage
* code complexiteit
* vulnerabilities / code smells (o.b.v. rules). Het is zelfs mogelijk om custom rules toe te voegen.

### Er zijn tevens plugins beschikbaar voor de meest gebruikte IDE's middels SonarLint:

* Eclipse
* IntelliJ IDEA
* Visual Studio
* VS Code
* Atom

Om zelf een idee te krijgen wat er zoal in SonarQube beschikbaar is, zie https://sonarcloud.io/projects en klik op een willekeurig project. Kies daarna voor 'Issues' en kijk wat er zoal aan problemen/aanbevelingen in deze repository aanwezig is.

## Installatie

Om zelf aan de slag te gaan met SonarQube moet je uiteraard wel beschikken over een software repository. Dit kan een bestaande repository van je project zijn, maar je kunt uiteraard ook dit voorbeeld project gebruiken.
Daarnaast is **Java 11** (JDK) vereist! **Zorg ervoor dat de juiste JDK is geinstalleerd! Heb je een 64-bit systeem, zorg dan ook dat je een 64-bit JDK hebt.**

### Controle correcte JDK (voor een 64-bit systeem):

* Voer op de commandline uit: 

````
java -d64 -version
````

* Dit moet iets opleveren wat lijkt op: Java HotSpot(TM) 64-Bit Server VM
* Mocht dit niet het geval zijn, de-installeer dan de oude JDK en installeer een recente, correcte versie

### Controle commandline gebruik Maven en Java:

* Voer onderstaande commando's op de commandline uit en valideer dat er geen fouten optreden: 

````
java -version
javac -version
mvn -version
````

* Dit moet iets opleveren wat lijkt op: java version "11.0.3" 2019-04-16 LTS, javac 11.0.3 en Apache Maven 3.6.1
* Mocht dit niet het geval zijn, installeer dan de [JDK](https://docs.oracle.com/en/java/javase/11/install/installation-jdk-microsoft-windows-platforms.html#GUID-A7E27B90-A28D-4237-9383-A58B416071CA) en/of [Maven](http://maven.apache.org/install.html) en zorg dat deze zijn toegevoegd aan je [PATH](https://docs.oracle.com/en/java/javase/11/install/installation-jdk-microsoft-windows-platforms.html#GUID-96EB3876-8C7A-4A25-9F3A-A2983FEC016A)

### Starten SonarQube Server:

* Maak een directory sonarqube aan:

````
C:\sonarqube\
````
of
````
/etc/sonarqube/
````

* Download de meest recente [LTS](https://www.sonarqube.org/downloads/lts/) COMMUNITY EDITION van SonarQube. Op het moment van schrijven is dit versie: https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-7.9.4.zip
* Pak deze uit in bovenstaande directory, zodat dit er als volgt 'uitziet':

````
C:\sonarqube\sonarqube-7.9.4
````
of
````
/etc/sonarqube/sonarqube-7.9.4
````
* Start de SonarQube server:

````
C:\sonarqube\sonarqube-7.9.4\bin\windows-x86-xx\StartSonar.bat
````
of
````
/etc/sonarqube/sonarqube-7.9.4/bin/[OS]/sonar.sh console
````

* In de prompt/terminal wordt nu de SonarQube Server gestart. Wacht tot de volgende melding er staat: **SonarQube is up**
* Ga nu met je browser naar: [http://localhost:9000/](http://localhost:9000/ "SonarQube Server") Hier moet nu de webinterface van SonarQube zichtbaar zijn.
  * de credentials zijn: admin/admin
  * het genereren van een token kan worden overgeslagen!
  
### Scanner installeren (indien Maven niet gebruikt wordt!)

* Indien je gebruik maakt van Maven kunnen onderstaande stappen worden overgeslagen
* Wanneer je **GEEN** gebruik maakt van Maven dien je de Sonar-Scanner handmatig te installeren, zie hiervoor: [sonar-scanner.md](./sonar-scanner.md).
* Volg de beschreven stappen op deze pagina en ga vervolgens weer verder met deze handleiding.

## Analyse

### Code analyseren met de SonarQube Scanner voor Maven

* Open een nieuwe terminal op de locatie waar de Project Object Model (pom.xml) staat en voer onderstaande uit:

````
mvn clean install sonar:sonar
````

* Wacht tot er **ANALYSIS SUCCESSFUL** staat

### Code analyse bekijken

* Ga naar [http://localhost:9000/](http://localhost:9000/ "SonarQube Server")
* Voer eventueel een harde refresh uit: Ctrl+F5
* Bekijk de resultaten van je (voorbeeld) project (en verbeter deze!)

### Afsluiten

**Sluit je terminal(s) niet zo maar af door op het kruisje te klikken, maar zorg ervoor dat de processen die je hier
gestart hebt ook netjes worden afgesloten (Ctrl+C)!!!**

## Jenkins / SonarQube / Docker setup

Voor een volledige setup met Docker, Jenkins en SonarQube, zie: [Docker Jenkins SonarQube](./docker-jenkins-sonarqube)