# Docker - Jenkins - SonarQube

**DON'T USE THIS SETUP FOR PRODUCTION!!**

**Running containers (and applications) with Docker implies running the Docker daemon. This daemon currently requires root privileges, and you should therefore be aware of some important details. First of all, only trusted users should be allowed to control your Docker daemon.**

This repo contains a `docker-compose.yml` file. Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.
Compose works in all environments: production, staging, development, testing, as well as CI workflows.

## Prerequisites

For this setup on `Windows` and `Mac`, you will require:

- [Docker and Docker Compose](https://store.docker.com/search?offering=community&type=edition)

For this setup on `Linux`:

- [Install Compose on Linux systems](https://docs.docker.com/compose/install/#install-compose)

## Getting started

1. `Fork and clone` or `copy` this repository on Gitlab to your own account - a fork is a copy of a project. Forking a repository allows you to make changes without affecting the original project
1. Clone the (just forked/copied) repository from your personal account and you are ready to start working with this setup in your own repository
1. Be sure to configure at least 4 GiB Memory (Docker Preferences - Advanced)

To get all docker containers up and running, open a terminal in the `docker-jenkins-sonarqube` directory and enter:

```docker
docker-compose up -d
```

This will setup a single network with:

- SonarQube
- Jenkins

When you run docker-compose up, the following happens:

- A network called `docker-jenkins-sonarqube` is created.
- A container is created based on a `jenkins image`. It joins the network `docker-jenkins-sonarqube` under the name `jenkins`.
- A container is created based on a `sonarqube image`. It joins the network `docker-jenkins-sonarqube` under the name `sonar`.

Each container for the service joins the network and is reachable by other containers on that network, and discoverable by the hostname identical to the container name.

### Unlocking Jenkins

When you first access a new Jenkins instance, you are asked to unlock it using an automatically-generated password:

1. browse to [http://localhost:8080](http://localhost:8080) and wait until the **Unlock Jenkins** page appears.
1. to access the logs, for the automatically-generated password to Unlock Jenkins enter in the terminal:
1. `docker ps` to find out the container id of `jenkins` and use it in the command below:
1. `docker logs container_id` replace container_id with your id
1. scroll the logs to **copy** the automatically-generated alphanumeric password (between the 2 sets of asterisks)
1. on the Unlock Jenkins page, paste this password into the Administrator password field and click Continue

### Customizing Jenkins with plugins

After unlocking Jenkins, the Customize Jenkins page appears. On this page, click **Install suggested plugins**. \
The setup wizard shows the progression of Jenkins being configured and the suggested plugins being installed. This process may take a few minutes.

### Creating the first administrator user

Finally, Jenkins asks you to create your first administrator user.

1. When the **Create First Admin User** page appears, specify your details (suggestion: use admin/admin as username/password) in the respective fields and click **Save and Finish**
1. When the **Jenkins is ready** page appears, click **Start using Jenkins**
1. If required, log in to Jenkins with the credentials of the user you just created and you’re ready to start using Jenkins!

### Create your initial Pipeline as a Jenkinsfile

You’re now ready to create your Pipeline that will automate building your Java application with Maven in Jenkins. Your Pipeline will be created as a `Jenkinsfile`, which is in the root of this repo.

This is the foundation of "**Pipeline-as-Code**", which treats the continuous delivery pipeline as a part of the application to be versioned and reviewed like any other code.

- Go back to [Jenkins](http://localhost:8080) again, log in again if necessary and choose from the Jenkins webinterface: 
- New item
- Choose "Pipeline
- enter a name (e.g. JenkinsSonar)
- OK
- Scroll down to the pipeline section and choose:
- Pipeline script from SCM
- SCM: Git
- Now follow the steps to configure a SSH Connection **OR** HTTP(S) connection below:

#### SSH Connection
If you wish to connect to the Git(lab) repo with SSH (recommended!) follow these steps:
- Repo URL - for example:
  - git@gitlab.com:YOUR-GITLAB-ACCOUNT-NAME/repo-which-contains-this-sample-project-and-Jenkinsfile.git
- Credentials - configure your credentials - choose Add
  - Choose SSH Username with private key
  - Enter Gitlab Username (=GITLAB-ACCOUNT-NAME)
  - Private Key - Enter directly (copy/paste private key from $HOME/.ssh/id_rsa)
  - Enter Description (e.g. SSH Gitlab credentials)
  - Add
- Save Configuration 
- Start build ('Start nu een bouwpoging or similar)
- Wait.....
- You should now see a passed Pipeline. If not check the console logs

#### HTTPS(S) Connection (SKIP IF YOU HAVE CONFIGURED SSH!!)
If you wish to connect to the Git(lab) repo with HTTP(S) follow these steps:
- Repo URL - for example:
  - https://gitlab.com/YOUR-GITLAB-ACCOUNT-NAME/repo-which-contains-this-sample-project-and-Jenkinsfile.git
- Credentials - configure your credentials - choose Add
  - Choose Username with Password (SSH is better!)
  - Enter Gitlab Username (=GITLAB-ACCOUNT-NAME)
  - Enter Gitlab Password
  - Enter Description (e.g. http credentials)
  - Add
- Save Configuration
- Start build ('Start nu een bouwpoging or similar)
- Wait.....
- You should now see a passed Pipeline. If not check the console logs

## But wait.. what just happened?!..

- You made a connection from your local Jenkins to an online Git(lab/hub) repo
- You created a Jenkinsfile which was read by Jenkins
- Jenkins downloaded and started a Maven container which becomes the agent that Jenkins uses to run your Pipeline project
- The Sonar analysis is pushed to your local Sonar instance and can be viewed by [http://localhost:9000/](http://localhost:9000/)

## Stop

To stop, simply enter:

```docker
docker-compose down
```

This will stop the containers, remove them and finally remove the network.